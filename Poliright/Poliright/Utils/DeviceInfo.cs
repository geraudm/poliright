﻿using System;
using System.Collections.Generic;
using System.Text;
using XLabs.Ioc;
using XLabs.Platform.Device;

namespace Poliright.Utils
{
    public static class DeviceInfo
    {
        public static int Width;
        public static int Height;
        
        public static void InitInfos()
        {
            var device = Resolver.Resolve<IDevice>();
            var oDisplay = device.Display;
            Width = oDisplay.Width;
            Height = oDisplay.Height;
        }
    }
}
