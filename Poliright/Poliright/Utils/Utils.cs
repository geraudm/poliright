﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

#if __ANDROID__
using Android.Views;
using Plugin.CurrentActivity;
#endif

namespace Poliright.Utils
{
    public static class Utils
    {
        public static bool CanExecute = true;
        public static void HideStatusBar()
        {
#if __ANDROID__

            Device.BeginInvokeOnMainThread(() =>
            {
                CrossCurrentActivity.Current.Activity.Window.AddFlags(WindowManagerFlags.Fullscreen);
                CrossCurrentActivity.Current.Activity.Window.ClearFlags(WindowManagerFlags.ForceNotFullscreen);
            });
#endif
        }

        public static void ShowStatusBar()
        {
#if __ANDROID__
            Device.BeginInvokeOnMainThread(() =>
            {
                CrossCurrentActivity.Current.Activity.Window.ClearFlags(WindowManagerFlags.Fullscreen);
                CrossCurrentActivity.Current.Activity.Window.AddFlags(WindowManagerFlags.ForceNotFullscreen);
            });
#endif
        }
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static async Task BlockButton(Action function, int delay)
        {
            if (CanExecute)
            {
                CanExecute = false;

                function.Invoke();

                await Task.Delay(delay);
                CanExecute = true;
            }
        }
        public static bool CanReachUrl(string url)
        {
            try
            {
                if (!string.IsNullOrEmpty(url))
                {
                    UriBuilder uriBuilder = new UriBuilder(url);
                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uriBuilder.Uri);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        Console.WriteLine("Broken - 404 Not Found");
                        return false;
                    }
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("URL appears to be good.");
                        return true;
                    }
                    else //There are a lot of other status codes you could check for...
                    {
                        Console.WriteLine("URL might be ok. Status: {0}.", response.StatusCode.ToString());
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Broken- Other error: {0}", ex.Message);
                return false;
            }
            return false;
        }
    }
}
