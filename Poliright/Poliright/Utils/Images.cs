﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poliright.Utils
{
    public class Images
    {
        public static readonly string IconImage = "Icon.png";
        public static readonly string PetersonTextImage = "peterson_text.png";
    }
}
