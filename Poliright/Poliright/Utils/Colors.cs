﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Poliright.Utils
{
    public class Colors
    {
        public static readonly Color MainBrightColor = Color.FromHex("fcfdff");
        public static readonly Color MenuBrightColor = Color.FromHex("f7f8f9");
        public static readonly Color MainColor = Color.FromHex("0099ff");
        public static readonly Color SelectedTabColor = MainColor;
        public static readonly Color StatusBarColor = MainColor;
    }
}
