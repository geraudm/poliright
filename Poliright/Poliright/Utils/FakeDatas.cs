﻿using System;
using System.Collections.ObjectModel;
using Poliright.Models;

namespace Poliright.Utils
{
    public static  class FakeDatas
    {

        public static ObservableCollection<PoliticianModel> GetPoliticians()
        {
            ObservableCollection<PoliticianModel> list = new ObservableCollection<PoliticianModel>();

            PoliticianModel p = new PoliticianModel();
            p.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            p.Name = "Emmanuel Macron";
            p.FeaturedImageUrl = "http://www.newstatesman.com/sites/default/files/styles/nodeimage/public/blogs_2017/06/macronwipeout.jpg";
            list.Add(p);

            PoliticianModel e = new PoliticianModel();
            e.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            e.Name = "Jean-Luc Mélenchon";
            e.FeaturedImageUrl = "http://www.kountrass.com/wp-content/uploads/2017/07/melenchon.jpg";
            list.Add(e);

            PoliticianModel r = new PoliticianModel();
            r.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            r.Name = "Nicolas Dupont-Aignan";
            r.FeaturedImageUrl = "http://www.lepoint.fr/images/2017/04/05/8082325lpw-8125870-article-sipa00794468000003-jpg_4206635_660x281.jpg";
            list.Add(r);

            PoliticianModel o = new PoliticianModel();
            o.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            o.Name = "François Hollande";
            o.FeaturedImageUrl = "http://libreopinionguinee.com/wp-content/uploads/2016/12/francois-hollande-sur-le-plateau-de-tf1-a-aubervilliers-le-6-novembre-2014-1_5145591.jpg";
            list.Add(o);
            return list;
        }

        public static ObservableCollection<SubjectModel> GetSubjects()
        {
            ObservableCollection<SubjectModel> list = new ObservableCollection<SubjectModel>();

            SubjectModel p = new SubjectModel();
            p.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            p.Name = "Écologie";
            p.FeaturedImageUrl = "http://www.planete-ecologie.com/wp-content/uploads/2016/05/Principes-fondamentaux-de-l%E2%80%99%C3%A9cologie-dans-le-champ-scientifique.jpg";
            list.Add(p);

            SubjectModel e = new SubjectModel();
            e.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            e.Name = "Éducation";
            e.FeaturedImageUrl = "http://www.siasat.com/wp-content/uploads/2017/12/education-858cc07046.jpg";
            list.Add(e);

            SubjectModel r = new SubjectModel();
            r.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            r.Name = "Travail";
            r.FeaturedImageUrl = "http://blog.talents.immo/wp-content/uploads/2017/06/1204169_travail-et-bien-etre-une-alliance-pour-le-meilleur-154776-1.jpg";
            list.Add(r);

            SubjectModel o = new SubjectModel();
            o.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            o.Name = "Immigration";
            o.FeaturedImageUrl = "http://pbs.twimg.com/media/DWbVX-9W4AAFiIA.jpg";
            list.Add(o);
            return list;
        }

        public static ObservableCollection<EventModel> GetEvents()
        {
            ObservableCollection<EventModel> list = new ObservableCollection<EventModel>();

            EventModel p = new EventModel();
            p.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            p.Name = "Présidentielles";
            p.FeaturedImageUrl = "http://france3-regions.francetvinfo.fr/hauts-de-france/sites/regions_france3/files/styles/top_big/public/assets/images/2017/05/07/000_o72ol-3042739.jpg?itok=tx9Vcevr";
            list.Add(p);

            EventModel e = new EventModel();
            e.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            e.Name = "Affaire Cahuzac";
            e.FeaturedImageUrl = "http://static.latribune.fr/full_width/119941/cahuzac.jpg";
            list.Add(e);

            EventModel r = new EventModel();
            r.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            r.Name = "Affaire Baupin";
            r.FeaturedImageUrl = "http://www.lepoint.fr/images/2016/05/09/3848822lpw-3850697-article-newzulu-jpg_3536367_660x281.jpg";
            list.Add(r);

            EventModel o = new EventModel();
            o.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            o.Name = "François Hollande";
            o.FeaturedImageUrl = "http://libreopinionguinee.com/wp-content/uploads/2016/12/francois-hollande-sur-le-plateau-de-tf1-a-aubervilliers-le-6-novembre-2014-1_5145591.jpg";
            //list.Add(o);
            return list;
        }



        public static PoliticianModel GetPolitician()
        {
            PoliticianModel p = new PoliticianModel();

            p.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            p.Name = "Emmanuel Macron";
            p.FeaturedImageUrl = "http://www.newstatesman.com/sites/default/files/styles/nodeimage/public/blogs_2017/06/macronwipeout.jpg";

            return p;
        }

        public static EventModel GetEvent()
        {
            EventModel e = new EventModel();

            e.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            e.Name = "Affaire Cahuzac";
            e.FeaturedImageUrl = "http://static.latribune.fr/full_width/119941/cahuzac.jpg";

            return e;
        }

        public static SubjectModel GetSubject()
        {
            SubjectModel e = new SubjectModel();

            e.Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit efficitur libero. Donec convallis sit amet justo vel feugiat. Etiam faucibus nibh placerat, feugiat dolor interdum, dictum neque. Sed quis nulla in urna consectetur vulputate eget eget nunc. Quisque vestibulum accumsan viverra. Quisque at hendrerit ante. Cras sed laoreet elit. Ut porta et dolor ac auctor. Donec hendrerit mauris vitae rhoncus congue. Nullam tincidunt dapibus dignissim. Suspendisse neque ipsum, elementum eu hendrerit non, tristique id ante. Aenean non ipsum sed nibh convallis faucibus.";
            e.Name = "Éducation";
            e.FeaturedImageUrl = "http://www.siasat.com/wp-content/uploads/2017/12/education-858cc07046.jpg";

            return e;
        }
    }
}
