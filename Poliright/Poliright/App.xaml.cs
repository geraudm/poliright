﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Poliright.Models;
using Poliright.Utils;
using Xamarin.Forms;

namespace Poliright
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

		    DeviceInfo.InitInfos();

		    var navPage = new NavigationPage(new Views.LoginPage());
            NavigationPage.SetHasNavigationBar(navPage, false);
            MainPage = navPage;
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
