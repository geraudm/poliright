﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poliright.Models;
using Poliright.ViewModels;
using Xamarin.Forms;

namespace Poliright.Views
{
	public partial class SubjectPage : ContentPage
	{
        SubjectViewModel ViewModel;

		public SubjectPage()
		{
			InitializeComponent();
		    NavigationPage.SetHasNavigationBar(this, false);
        }
        public SubjectPage(SubjectModel subject)
        {
            InitializeComponent();
            BindingContext = ViewModel = new SubjectViewModel(subject);
            NavigationPage.SetHasNavigationBar(this, false);
        }
	}
}
