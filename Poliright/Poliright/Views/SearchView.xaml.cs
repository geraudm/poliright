﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poliright.Models;
using Poliright.ViewModels;
using Xamarin.Forms;

namespace Poliright.Views
{
	public partial class SearchView : Grid
	{
        SearchViewModel ViewModel;

		public SearchView()
		{
            InitializeComponent();
            BindingContext = ViewModel = new SearchViewModel();
		}

        private void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            // Désactiver la selection
            if (((ListView)sender).SelectedItem != null)
            {
                ContentPage page = new ContentPage();
                if (((ListView) sender).SelectedItem.GetType() == typeof(SubjectModel))
                {
                    SubjectModel s = (SubjectModel)((ListView)sender).SelectedItem;
                    page = new SubjectPage(s);
                }
                if (((ListView)sender).SelectedItem.GetType() == typeof(EventModel))
                {
                    EventModel em = (EventModel)((ListView)sender).SelectedItem;
                    page = new EventPage(em);
                }
                if (((ListView)sender).SelectedItem.GetType() == typeof(PoliticianModel))
                {
                    PoliticianModel p = (PoliticianModel)((ListView)sender).SelectedItem;
                    page = new PoliticianPage(p);
                }
                Navigation.PushAsync(page);
            }
            ((ListView)sender).SelectedItem = null;
        }
	}
}
