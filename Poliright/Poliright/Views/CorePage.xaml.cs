﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poliright.ViewModels;
using Xamarin.Forms;

namespace Poliright.Views
{
    public partial class CorePage : ContentPage
    {
        public CoreViewModel ViewModel;

        public CorePage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            BindingContext = ViewModel = CoreViewModel.Instance;
            InitButtons();


            Carousel.PositionSelected += CarouselPositionSelected;

            Carousel.ItemsSource = new List<DataTemplate>()
            {
                new DataTemplate(() => { return new SearchView(); }),
                new DataTemplate(() => { return new ListPoliticianView(); }),
                new DataTemplate(() => { return new ListEventView(); }),
                new DataTemplate(() => { return new ListSubjectView(); })
            };
            CursorLayout.HorizontalOptions = LayoutOptions.Start;

            GoToPage(0);
        }

        private void CarouselPositionSelected(object sender,
            CarouselView.FormsPlugin.Abstractions.PositionSelectedEventArgs e)
        {
            GoToPage(e.NewValue);
        }

        public void InitButtons()
        {
            SearchViewButton.GestureRecognizers.Add(new TapGestureRecognizer((view) => GoToPage(0)));
            PoliticianListViewButton.GestureRecognizers.Add(new TapGestureRecognizer((view) => GoToPage(1)));
            EventListViewButton.GestureRecognizers.Add(new TapGestureRecognizer((view) => GoToPage(2)));
            SubjectListViewButton.GestureRecognizers.Add(new TapGestureRecognizer((view) => GoToPage(3)));
        }

        public void GoToPage(int pos)
        {
            if (Carousel.Position != pos)
                Carousel.Position = pos;
            CursorLayout.TranslateTo(Carousel.Position * ControlGrid.Width / 4, 0, 250U, Easing.SinInOut);
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            try
            {
                // Laisser comme ça sinon ça peut crasher sur ios
                base.OnSizeAllocated(width, height);
            }
            catch (Exception ex)
            {
            }
            try
            {
                if (width > height)
                {
                    Debug.WriteLine("Landscape");
                    Padding = new Thickness(0, Device.OnPlatform(0, 0, 0), 0, 0);
                }
                if (width <= height)
                {
                    Debug.WriteLine("Portrait");
                    Padding = new Thickness(0, Device.OnPlatform(20, 0, 0), 0, 0);
                }
                CursorLayout.WidthRequest = ControlGrid.Width / 4;
                CursorLayout.TranslateTo(Carousel.Position * ControlGrid.Width / 4, 0, 250U, Easing.SinInOut);
            }
            catch (Exception ex)
            {
            }
        }

        private async void OpenProfilePage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ProfilePage());
        }
    }
}
