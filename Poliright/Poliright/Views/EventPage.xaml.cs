﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poliright.Models;
using Poliright.ViewModels;
using Xamarin.Forms;

namespace Poliright.Views
{
	public partial class EventPage : ContentPage
    {
        EventViewModel ViewModel;

		public EventPage()
		{
			InitializeComponent();
		    NavigationPage.SetHasNavigationBar(this, false);
        }
        public EventPage(EventModel eventModel)
        {
            InitializeComponent();
            BindingContext = ViewModel = new EventViewModel(eventModel);
            NavigationPage.SetHasNavigationBar(this, false);
        }
	}
}
