﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poliright.Models;
using Poliright.ViewModels;
using Xamarin.Forms;

namespace Poliright.Views
{
	public partial class ListSubjectView : Grid
    {
        ListSubjectViewModel ViewModel;

		public ListSubjectView()
		{
            InitializeComponent();
            BindingContext = ViewModel = new ListSubjectViewModel();
        }

        private async void OpenSubjectPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SubjectPage());
        }
        private void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            // Désactiver la selection
            if (((ListView)sender).SelectedItem != null)
            {
                SubjectModel s = (SubjectModel)((ListView)sender).SelectedItem;
                Navigation.PushAsync(new SubjectPage(s));
            }
            ((ListView)sender).SelectedItem = null;
        }
    }
}
