﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Poliright.Views
{
	public partial class LoginPage : ContentPage
	{
		public LoginPage()
		{
			InitializeComponent();
		    NavigationPage.SetHasNavigationBar(this, false);
        }

	    private void OpenApp(object sender, EventArgs e)
	    {
	        var navPage = new NavigationPage(new CorePage());
	        NavigationPage.SetHasNavigationBar(navPage, false);
	        App.Current.MainPage = navPage;
        }
	}
}
