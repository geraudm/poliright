﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poliright.Models;
using Poliright.ViewModels;
using Xamarin.Forms;

namespace Poliright.Views
{
	public partial class ListEventView : Grid
    {
        ListEventViewModel ViewModel;
		public ListEventView()
		{
            InitializeComponent();
            BindingContext = ViewModel = new ListEventViewModel();
        }

        private async void OpenEventPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EventPage());
        }
        private void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            // Désactiver la selection
            if (((ListView)sender).SelectedItem != null)
            {
                EventModel em = (EventModel)((ListView)sender).SelectedItem;
                Navigation.PushAsync(new EventPage(em));
            }
            ((ListView)sender).SelectedItem = null;
        }
    }
}
