﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poliright.Models;
using Poliright.ViewModels;
using Xamarin.Forms;

namespace Poliright.Views
{
	public partial class ListPoliticianView : Grid
    {
        ListPoliticianViewModel ViewModel;

		public ListPoliticianView()
		{
			InitializeComponent();
            BindingContext = ViewModel = new ListPoliticianViewModel();
        }

        private async void OpenPoliticianPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PoliticianPage());
        }
        private void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            // Désactiver la selection
            if (((ListView)sender).SelectedItem != null)
            {
                PoliticianModel p = (PoliticianModel)((ListView)sender).SelectedItem;
                Navigation.PushAsync(new PoliticianPage(p));
            }
            ((ListView)sender).SelectedItem = null;
        }
    }
}
