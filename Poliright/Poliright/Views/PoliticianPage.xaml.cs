﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poliright.Models;
using Poliright.ViewModels;
using Xamarin.Forms;

namespace Poliright.Views
{
	public partial class PoliticianPage : ContentPage
	{
        PoliticianViewModel ViewModel;

		public PoliticianPage()
		{
			InitializeComponent();
		    NavigationPage.SetHasNavigationBar(this, false);
        }
        public PoliticianPage(PoliticianModel politician)
        {
            InitializeComponent();
            BindingContext = ViewModel = new PoliticianViewModel(politician);
            NavigationPage.SetHasNavigationBar(this, false);
        }
	}
}
