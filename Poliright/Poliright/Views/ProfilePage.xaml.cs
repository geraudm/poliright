﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Poliright.Views
{
	public partial class ProfilePage : ContentPage
	{
		public ProfilePage()
		{
			InitializeComponent();
		    NavigationPage.SetHasNavigationBar(this, false);
        }

        void SaveAndClose(object sender, System.EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}
