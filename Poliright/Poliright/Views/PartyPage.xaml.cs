﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Poliright.Views
{
	public partial class PartyPage : ContentPage
	{
		public PartyPage()
		{
			InitializeComponent();
		    NavigationPage.SetHasNavigationBar(this, false);
        }
	}
}
