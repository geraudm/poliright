﻿using System;
using System.Collections.Generic;
using System.Text;
using Poliright.Models;

namespace Poliright.ViewModels
{
    public class SubjectViewModel : BaseViewModel
    {
        public SubjectViewModel(SubjectModel subject)
        {
            Subject = subject;
        }

        private SubjectModel _subject;
        public SubjectModel Subject
        {
            get { return _subject; }
            set { SetProperty(ref _subject, value); }
        }
    }
}
