﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Poliright.Models;

namespace Poliright.ViewModels
{
    public class ListSubjectViewModel : BaseViewModel
    {
        public ListSubjectViewModel()
        {
            Subjects = new ObservableCollection<SubjectModel>();
            Subjects = Utils.FakeDatas.GetSubjects();
        }

        private ObservableCollection<SubjectModel> _subjects;
        public ObservableCollection<SubjectModel> Subjects
        {
            get { return _subjects; }
            set { SetProperty(ref _subjects, value); }
        }
    }
}
