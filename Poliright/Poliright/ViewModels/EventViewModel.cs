﻿using System;
using System.Collections.Generic;
using System.Text;
using Poliright.Models;

namespace Poliright.ViewModels
{
    public class EventViewModel : BaseViewModel
    {
        public EventViewModel(EventModel eventModel)
        {
            Event = eventModel;
        }

        private EventModel _event;
        public EventModel Event
        {
            get { return _event; }
            set { SetProperty(ref _event, value); }
        }
    }
}
