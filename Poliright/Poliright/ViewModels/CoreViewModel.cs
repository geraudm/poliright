﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poliright.ViewModels
{
    public class CoreViewModel : BaseViewModel
    {
        private static volatile CoreViewModel instance;
        private static object syncRoot = new Object();
        public static CoreViewModel Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new CoreViewModel();
                    }
                }
                return instance;
            }
        }

        private int _position;
        public int Position
        {
            get { return _position; }
            set { SetProperty(ref _position, value); }
        }
        public CoreViewModel()
        {
            Position = 0;
        }
    }
}
