﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Poliright.Models;

namespace Poliright.ViewModels
{
    public class ListEventViewModel : BaseViewModel
    {
        public ListEventViewModel()
        {
            Events = new ObservableCollection<EventModel>();
            Events = Utils.FakeDatas.GetEvents();
        }

        private ObservableCollection<EventModel> _events;
        public ObservableCollection<EventModel> Events
        {
            get { return _events; }
            set { SetProperty(ref _events, value); }
        }
    }
}
