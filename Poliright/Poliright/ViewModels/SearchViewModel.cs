﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Poliright.Models;

namespace Poliright.ViewModels
{
    public class SearchViewModel : BaseViewModel
    {
        public SearchViewModel()
        {
            LastSeen = new ObservableCollection<BasePoliticalModel>();
            LastSeen.Add(Utils.FakeDatas.GetEvent());
            LastSeen.Add(Utils.FakeDatas.GetPolitician());
            LastSeen.Add(Utils.FakeDatas.GetSubject());
        }

        private ObservableCollection<BasePoliticalModel> _lastSeen;
        public ObservableCollection<BasePoliticalModel> LastSeen
        {
            get { return _lastSeen; }
            set { SetProperty(ref _lastSeen, value); }
        }
    }
}
