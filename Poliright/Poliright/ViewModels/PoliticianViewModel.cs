﻿using System;
using System.Collections.Generic;
using System.Text;
using Poliright.Models;

namespace Poliright.ViewModels
{
    public class PoliticianViewModel : BaseViewModel
    {
        public PoliticianViewModel(PoliticianModel politician)
        {
            Politician = politician;
        }

        private PoliticianModel _politician;
        public PoliticianModel Politician
        {
            get { return _politician; }
            set { SetProperty(ref _politician, value); }
        }
    }
}
