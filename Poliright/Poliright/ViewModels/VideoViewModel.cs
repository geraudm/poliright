﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Akavache;
using Peterson.Models;
using Peterson.Utils;
using Peterson.Views.Popup;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using XLabs;

namespace Peterson.ViewModels
{
    public class VideoViewModel : BaseViewModel
    {
        private static volatile VideoViewModel instance;
        private static object syncRoot = new Object();
        public static VideoViewModel Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new VideoViewModel();
                    }
                }
                return instance;
            }
        }
        private ObservableCollection<VideosModel> VideoCategorizedTmp = new ObservableCollection<VideosModel>();
        private ObservableCollection<VideosModel> _videoCategorized = new ObservableCollection<VideosModel>();
        public ObservableCollection<VideosModel> VideoCategorized
        {
            get => _videoCategorized;
            set => SetProperty(ref _videoCategorized, value);
        }
        public ICommand OpenVideoPlayer { get; protected set; }
        public VideoViewModel()
        {
            CrossConnectivity.Current.ConnectivityChanged += Current_ConnectivityChanged;
            OpenVideoPlayer = new Command<VideoModel>(OpenVideoPlayerFunction);

            var enumLenght = Enum.GetNames(typeof(VideoModel.VideoCategory)).Length;
            for (int i = 0; i < enumLenght; i++)
            {
                VideoCategorized.Add(new VideosModel());
                VideoCategorizedTmp.Add(new VideosModel());
            }


            /*
            Task.Run(async delegate
            {
                try
                {
                    Promesses =
                        await BlobCache.UserAccount.GetObject<ObservableCollection<PromessModel>>(
                            Strings.AkavacheKeyPromise);
                }
                catch (KeyNotFoundException ex)
                {
                }
            });
*/

            // Gérer pour les updates, comment on fait pour ajouter des videos en dur dans la db ?

            AddVideos();
            Task.Run(async delegate
            {
                try
                {
                    VideoCategorized =
                        await BlobCache.UserAccount.GetObject<ObservableCollection<VideosModel>>(
                            Strings.AkavacheKeyVideos);
                    // Update videos à partir du Tmp
                    await UpdateVideos();
                }
                catch (KeyNotFoundException ex)
                {
                    // Se lance que la première fois, quand il n'y a pas encore ce champs dans la db
                    await BlobCache.UserAccount.InsertObject(Strings.AkavacheKeyVideos, VideoCategorizedTmp);
                    VideoCategorized =
                        await BlobCache.UserAccount.GetObject<ObservableCollection<VideosModel>>(
                            Strings.AkavacheKeyVideos);
                }
                LoadVideos();
            }).Wait();


            // Ajoute les vidéos dans la liste
            // Load le contenu de chaque vidéo, lien yt, titre etc
        }

        public async Task UpdateVideos()
        {
            for (int i = 0; i < VideoCategorizedTmp.Count; i++)
            {
                foreach (var videoCategorized in VideoCategorized)
                {
                    foreach (var video in videoCategorized.Videos)
                    {
                        // Do replace video
                        if (VideoCategorizedTmp[i].Videos.Any(model => model.VideoId == video.VideoId))
                        {
                            for (int j = 0; j < VideoCategorizedTmp[i].Videos.Count; j++)
                            {
                                if (VideoCategorizedTmp[i].Videos[j].VideoId == video.VideoId)
                                    VideoCategorizedTmp[i].Videos[j] = video;
                            }
                        }

                    }
                }
            }
            await BlobCache.UserAccount.InsertObject(Strings.AkavacheKeyVideos, VideoCategorizedTmp);
            VideoCategorized =
                await BlobCache.UserAccount.GetObject<ObservableCollection<VideosModel>>(
                    Strings.AkavacheKeyVideos);
        }

        public void AddVideos()
        {
            /*
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.].Videos.Add(new VideoModel(""));
             */

            // Responsibility
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Responsibility].Videos.Add(new VideoModel("wLvd_ZbX1w0"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Responsibility].Videos.Add(new VideoModel("GhrpcwIZdiQ"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Responsibility].Videos.Add(new VideoModel("h0aA2dQqt08"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Responsibility].Videos.Add(new VideoModel("Bi-RT1jYP_A"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Responsibility].Videos.Add(new VideoModel("QctT0Oc_uQQ"));

            // Men
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Men].Videos.Add(new VideoModel("Z2yzUxpmDM0"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Men].Videos.Add(new VideoModel("JjfClL6nogo"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Men].Videos.Add(new VideoModel("oTjZvlKfNtk"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Men].Videos.Add(new VideoModel("N-o2nnDF_So"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Men].Videos.Add(new VideoModel("oucrUXu38-E"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Men].Videos.Add(new VideoModel("qNODvgBsQiY"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Men].Videos.Add(new VideoModel("njoBZ0PDkPE"));

            // Women
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Women].Videos.Add(new VideoModel("kj7VgBnQNUc"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Women].Videos.Add(new VideoModel("BhPnxmw4xNA"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Women].Videos.Add(new VideoModel("NV2yvI4Id9Q"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Women].Videos.Add(new VideoModel("hU7AewaHr0Q"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Women].Videos.Add(new VideoModel("ILNRQ7ekGjo"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Women].Videos.Add(new VideoModel("Prb3t_S268A"));

            // Men & Women
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.MenAndWomen].Videos.Add(new VideoModel("iVm9Vbcmaso"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.MenAndWomen].Videos.Add(new VideoModel("ewvqEqIXdhU"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.MenAndWomen].Videos.Add(new VideoModel("LSXEHsYf8uQ"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.MenAndWomen].Videos.Add(new VideoModel("hFBk1iLMPds"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.MenAndWomen].Videos.Add(new VideoModel("dL3Hrwg3A3w"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.MenAndWomen].Videos.Add(new VideoModel("fbSb-vBkcHo"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.MenAndWomen].Videos.Add(new VideoModel("JUxY_5-N81Q"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.MenAndWomen].Videos.Add(new VideoModel("LH16ympCb7Q"));


            // Interviews
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Interviews].Videos.Add(new VideoModel("v-hIVnmUdXM"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Interviews].Videos.Add(new VideoModel("VJMCQ94t98k"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Interviews].Videos.Add(new VideoModel("8PB4h59nZP4"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Interviews].Videos.Add(new VideoModel("04wyGK6k6HE"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Interviews].Videos.Add(new VideoModel("USg3NR76XpQ"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.Interviews].Videos.Add(new VideoModel("6G59zsjM2UI"));

            // Personality Traits
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.PersonalityTraits].Videos.Add(new VideoModel("6mjD3K96MVY"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.PersonalityTraits].Videos.Add(new VideoModel("x6v6PVyqhMY"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.PersonalityTraits].Videos.Add(new VideoModel("BkGTaXHZBLA"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.PersonalityTraits].Videos.Add(new VideoModel("HcwYXjNo-2Q"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.PersonalityTraits].Videos.Add(new VideoModel("ngaaghmfUis"));

            // Biblical Series
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.BiblicalSeries].Videos.Add(new VideoModel("f-wWBGo6a2w"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.BiblicalSeries].Videos.Add(new VideoModel("hdrLQ7DpiWs"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.BiblicalSeries].Videos.Add(new VideoModel("R_GPAl_q2QQ"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.BiblicalSeries].Videos.Add(new VideoModel("Ifi5KkXig3s"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.BiblicalSeries].Videos.Add(new VideoModel("44f3mxcsI50"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.BiblicalSeries].Videos.Add(new VideoModel("wNjbasba-Qw"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.BiblicalSeries].Videos.Add(new VideoModel("6gFjB9FTN58"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.BiblicalSeries].Videos.Add(new VideoModel("UoQdp2prfmM"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.BiblicalSeries].Videos.Add(new VideoModel("GmuzUZTJ0GA"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.BiblicalSeries].Videos.Add(new VideoModel("3Y6bCqT85Pc"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.BiblicalSeries].Videos.Add(new VideoModel("SKzpj0Ev8Xs"));
            VideoCategorizedTmp[(int)VideoModel.VideoCategory.BiblicalSeries].Videos.Add(new VideoModel("-yUP40gwht0"));
        }

        public async void LoadVideos()
        {
            if (!CrossConnectivity.Current.IsConnected)
                return;
            foreach (var videoCategory in VideoCategorized)
                foreach (var video in videoCategory.Videos)
                    video.InitModel();
        }

        private void Current_ConnectivityChanged(object sender, Plugin.Connectivity.Abstractions.ConnectivityChangedEventArgs e)
        {
            LoadVideos();
        }

        public VideoPlayerPopup videoPlayerPopup = null;
        public async void OpenVideoPlayerFunction(VideoModel video)
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                // Msg erreur pas de co
                return;
            }
            // if (!String.IsNullOrEmpty(video.StreamUrl))
            await Utils.Utils.BlockButton(delegate
            {
                try
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        PopupNavigation.Instance.PushAsync(new VideoPlayerPopup(video));
                        //App.CorePage.Navigation.PushPopupAsync(new VideoPlayerPopup(video));
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }, 500);
        }
    }
}
