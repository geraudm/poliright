﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Poliright.Models;

namespace Poliright.ViewModels
{
    public class ListPoliticianViewModel : BaseViewModel
    {
        public ListPoliticianViewModel()
        {
            Politicians = new ObservableCollection<PoliticianModel>();
            Politicians = Utils.FakeDatas.GetPoliticians();
        }

        private ObservableCollection<PoliticianModel> _politicians;
        public ObservableCollection<PoliticianModel> Politicians
        {
            get { return _politicians; }
            set { SetProperty(ref _politicians, value); }
        }
    }
}
