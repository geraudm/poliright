﻿using System;
using System.Collections.ObjectModel;

namespace Poliright.Models
{
    public class EventModel : BasePoliticalModel
    {
        public EventModel()
        {
            Subjects = new ObservableCollection<SubjectModel>();
            Politicians = new ObservableCollection<PoliticianModel>();
        }

        private ObservableCollection<SubjectModel> _subjects;
        public ObservableCollection<SubjectModel> Subjects
        {
            get { return _subjects; }
            set { SetProperty(ref _subjects, value); }
        }

        private ObservableCollection<PoliticianModel> _politicians;
        public ObservableCollection<PoliticianModel> Politicians
        {
            get { return _politicians; }
            set { SetProperty(ref _politicians, value); }
        }
    }
}
