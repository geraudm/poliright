﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Peterson.Models
{
    public class MemeModel : BaseModel
    {
        private string _filename;
        public string Filename
        {
            get { return _filename; }
            set { SetProperty(ref _filename, value); }
        }
        public MemeModel(string filename)
        {
            Filename = filename;
        }
    }
}
