﻿using System;
using System.Collections.ObjectModel;

namespace Poliright.Models
{
    public class PoliticianModel : BasePoliticalModel
    {
        public PoliticianModel()
        {
            Events = new ObservableCollection<EventModel>();
        }

        private PartyModel _party;
        public PartyModel Party
        {
            get { return _party; }
            set { SetProperty(ref _party, value); }
        }

        private ObservableCollection<EventModel> _events;
        public ObservableCollection<EventModel> Events
        {
            get { return _events; }
            set { SetProperty(ref _events, value); }
        }
    }
}
