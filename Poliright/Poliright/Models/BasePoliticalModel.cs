﻿using System;
namespace Poliright.Models
{
    public class BasePoliticalModel : BaseModel
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        private string _featuredImageUrl;
        public string FeaturedImageUrl
        {
            get { return _featuredImageUrl; }
            set { SetProperty(ref _featuredImageUrl, value); }
        }
    }
}
