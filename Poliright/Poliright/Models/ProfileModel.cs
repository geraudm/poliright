﻿using System;

namespace Poliright.Models
{
    public class ProfileModel : BaseModel
    {
        public ProfileModel()
        {
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }
    }
}
