﻿using System;
using System.Collections.ObjectModel;

namespace Poliright.Models
{
    public class SubjectModel : BasePoliticalModel
    {
        public SubjectModel()
        {
            Events = new ObservableCollection<EventModel>();
            Politicians = new ObservableCollection<PoliticianModel>();
        }

        private ObservableCollection<EventModel> _events;
        public ObservableCollection<EventModel> Events
        {
            get { return _events; }
            set { SetProperty(ref _events, value); }
        }

        private ObservableCollection<PoliticianModel> _politicians;
        public ObservableCollection<PoliticianModel> Politicians
        {
            get { return _politicians; }
            set { SetProperty(ref _politicians, value); }
        }
    }
}
