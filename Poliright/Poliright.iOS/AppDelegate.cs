﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarouselView.FormsPlugin.iOS;
using FFImageLoading.Forms.Touch;
using Foundation;
using UIKit;
using XLabs.Ioc;
using XLabs.Platform.Device;

namespace Poliright.iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		//
		// This method is invoked when the application has loaded and is ready to run. In this 
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
		    global::Xamarin.Forms.Forms.Init();

            /* var manager = BITHockeyManager.SharedHockeyManager;
		    manager.Configure("c9b194502aed463797b379892609110b");
		    manager.StartManager();
		    manager.Authenticator.AuthenticateInstallation(); */ // This line is obsolete in crash only builds

		    CachedImageRenderer.Init();

            var container = new XLabs.Ioc.SimpleContainer();
            try
		    {
		        container.Register<IDevice>(t => AppleDevice.CurrentDevice);
		        Resolver.SetResolver(container.GetResolver());
		        CarouselViewRenderer.Init();
		    }
		    catch (Exception e) { }

		    UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, false);
		    UIApplication.SharedApplication.SetStatusBarHidden(false, UIStatusBarAnimation.Slide);

		    LoadApplication(new Poliright.App());

		    return base.FinishedLaunching(app, options);
        }
	}
}
