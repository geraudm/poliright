﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.Graphics;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using CarouselView.FormsPlugin.Android;
using FFImageLoading.Forms.Droid;
using Poliright.Utils;
using Xamarin.Forms.Platform.Android;
using XLabs.Ioc;
using XLabs.Platform.Device;

namespace Poliright.Droid
{
	[Activity (Label = "Poliright", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]

    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        SimpleContainer container = null;
        protected override void OnCreate (Bundle bundle)
		{
		    try
		    {
		        container = new SimpleContainer();
		        container.Register<IDevice>(t => AndroidDevice.CurrentDevice);
		        Resolver.SetResolver(container.GetResolver());
		    }
		    catch (Exception e) { }

		    CachedImageRenderer.Init(true);
		    CarouselViewRenderer.Init();

		    base.OnCreate(bundle);
            
		    global::Xamarin.Forms.Forms.Init(this, bundle);
            
		    if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
		    {
		        this.Window.ClearFlags(WindowManagerFlags.TranslucentNavigation);
		        this.Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
		        this.Window.SetStatusBarColor(Colors.StatusBarColor.ToAndroid());
		        this.Window.SetTitleColor(Color.White);
		    }
		    LoadApplication(new Poliright.App());
        }
	}
}

