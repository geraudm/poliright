﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Content.PM;
using Poliright.Droid;

namespace Peterson.Droid
{
    /*
     * SplashScreen Android
     */
    [Activity(Label = "Poliright", Icon = "@drawable/icon", Theme = "@style/Theme.Splash", NoHistory = true,
        MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            var intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
        }
    }
}