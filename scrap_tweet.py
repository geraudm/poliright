#!/usr/bin/env python
# encoding: utf-8

import tweepy #https://github.com/tweepy/tweepy
import csv
import sys

#Twitter API credentials
consumer_key = "HPNjVkdg3B657QzfmeBmVYUmg"
consumer_secret = "7Vt3iiDQRa1xHckf53x112J1kdSm5lL0c5Nkfl5GjGloH2PtbL"
access_key = "602905322-awCDd1aiIDcbnCwcegfJes03wPANOa1PQusDQa1q"
access_secret = "7o5UixXEmYBg7zvYk1J1QOlRxqYSnQ6qNL2WzJqLjAUhM"


def get_all_tweets(screen_name):
	#Twitter only allows access to a users most recent 3240 tweets with this method
	
	#authorize twitter, initialize tweepy
	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_key, access_secret)
	api = tweepy.API(auth)
	
	#initialize a list to hold all the tweepy Tweets
	alltweets = []	
	
	#make initial request for most recent tweets (200 is the maximum allowed count)
	new_tweets = api.user_timeline(screen_name = screen_name,count=200)
	
	#save most recent tweets
	alltweets.extend(new_tweets)
	
	#save the id of the oldest tweet less one
	oldest = alltweets[-1].id - 1
	
	#keep grabbing tweets until there are no tweets left to grab
	while len(new_tweets) > 0:
		print "getting tweets before %s" % (oldest)
		
		#all subsiquent requests use the max_id param to prevent duplicates
		new_tweets = api.user_timeline(screen_name = screen_name,count=200,max_id=oldest)
		
		#save most recent tweets
		alltweets.extend(new_tweets)
		
		#update the id of the oldest tweet less one
		oldest = alltweets[-1].id - 1
		
		print "...%s tweets downloaded so far" % (len(alltweets))
	
	#transform the tweepy tweets into a 2D array that will populate the csv	
	outtweets = [[tweet.id_str, tweet.created_at, tweet.text.encode("utf-8")] for tweet in alltweets]
	
	#write the csv	
	with open('%s_tweets.csv' % screen_name, 'wb') as f:
		writer = csv.writer(f)
		writer.writerow(["id","created_at","text"])
		writer.writerows(outtweets)
	
	pass


def read_tweets(screen_name):
	with open('%s_tweets.csv' % screen_name) as csvfile:
	    reader = csv.DictReader(csvfile)
	    for row in reader:
	        print(row['text'])

if __name__ == '__main__':
	if len(sys.argv) == 1:
		print("Enter a twitter handle")
		#read_tweets("MLP_officiel")
	else:
		#pass in the username of the account you want to download
		get_all_tweets(sys.argv[1])